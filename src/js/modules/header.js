// const { listenerCount } = require("gulp")
define(['jquery'],()=>{
    class Header{
        constructor(){
            this.loadHTML().then(()=>{
                this.calcCartCount()
            })
            
        }
        loadHTML(){
            return new Promise(resolve =>{
                $('header').load('/html/modules/header.html',resolve)
            })
        }
        calcCartCount(){
            let count = 0
            let cart = localStorage.getItem('cart')
            if(cart){
                cart = JSON.parse(cart)
                count = cart.reduce((total,shop)=>{
                    return total +shop.count
                },0)
            }
            $('#shopNum').html(count)
        }
    }
    return new Header()
})