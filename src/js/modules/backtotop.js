define(['jquery'],()=>{
    class Backtotop{
        constructor(){
            this.backToTop()
        }
        backToTop(){
            $('#back-to-top').hide()
            $(window).scroll(()=>{
                if($(window).scrollTop()>$(window).height()/2){
                    $('#back-to-top').fadeIn(111);
                }else{
                    $('#back-to-top').fadeOut(111);
                }
            });
            $('#back-to-top').on('click',()=>{
                $('body,html').animate({
                    scrollTop: 0
                  },
                  500);
                return false;
            })
        }
    }
    return new Backtotop()
})