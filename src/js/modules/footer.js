define(['jquery'],()=>{
    class Footer{
        constructor(){
            this.loadHTML()
        }
        loadHTML(){
            $('footer').load('/html/modules/footer.html')
        }
    }
    return new Footer()
})