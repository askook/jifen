require(['./config'],()=>{
    require(['footer'],()=>{
        class Reg{
            constructor(){
                this.login()
            }
            login(){
                $('.login-btn').on('click',function(){
                    const userName = $('.input1').val()
                    const passWord = $('.input2').val()
                    if(userName&&passWord){
                        let userArr = localStorage.getItem('userArr')
                        if(userArr){
                            userArr = JSON.parse(userArr)
                            const isIn = userArr.some(userArr=>{
                                return userArr.userName === userName
                            })
                            if(isIn){
                                userArr = userArr.filter(userArr =>{
                                    if(userArr.userName != userName) 
                                    return userArr
                                    if(userArr.passWord === passWord){
                                        alert('登陆成功，即将去首页')
                                        window.location.href="/index.html"
                                    }else{
                                        alert('密码错误')
                                    }
                                })
                            }else{
                                alert('用户不存在，先去注册吧')
                                window.location.href="/html/reg.html"
                            }
                        }else{
                            alert('当前还没有用户，快去注册吧')
                            window.location.href="/html/reg.html"
                        }
                    }else{
                        alert('账号或者密码未输入')
                    }
                })
            }
        }
        new Reg()
    })
})