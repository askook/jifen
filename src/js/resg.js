require(['./config'],()=>{
    require(['footer'],()=>{
        class Reg{
            constructor(){
                this.reg()
            }
            reg(){
                $('.regBtn').on('click',function(){
                    const userName = $('.input1').val()
                    const passWord = $('.input2').val()
                    if(userName&&passWord){
                        let userArr = localStorage.getItem('userArr')
                        const userObj = {
                            userName:userName,
                            passWord:passWord
                        }
                        if(userArr){
                            userArr = JSON.parse(userArr)
                            const isIn = userArr.some(userArr=>{
                                return userArr.userName === userName
                            })
                            if(isIn){
                                alert('已经存在相同用户，换个名字吧')
                            }else{
                                userArr.push(userObj)
                                localStorage.setItem('userArr',JSON.stringify(userArr))
                                alert('注册成功，即将去登录页面')
                                window.location.href="/html/login.html"
                            }
                        } 
                        else{
                            const userArr = []
                            userArr.push(userObj)
                            localStorage.setItem('userArr',JSON.stringify(userArr))
                            alert('注册成功，即将去登录页面')
                            window.location.href="/html/login.html"
                        }
                    }else{
                        alert('账号或者密码未输入')
                    }
                })
            }
        }
        new Reg()
    })
})