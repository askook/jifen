require(['./config'],()=>{
    require(['template','header','footer','backtotop'],(template)=>{
        class Index{
            constructor(){
                this.getTheme()
                this.getHotList()
                this.getNewList()
            }
            getTheme(){
                //请求本地json渲染
                $.get('/libs/json/homeTheme.json',resp=>{
                    //template方法第一个参数传ID名
                    //第二个参数是对象，对象里写模板所需要的数据
                    //对象的属性名是模板里需要的变量名，属性值是从后端拿到的值
                    const html = template('homeThemeTemplate',{
                        list:resp
                    })
                    $('.homeThemeTemplate').html(html)
                }),
                $.get('http://xiongmaoyouxuan.com/api/tabs',res=>{
                    if(res.code === 200){
                        const {list} = res.data
                        $('.homeTheme1Template').html(template('homeTheme1Template',{list:list.slice(1,6)}))

                    }
                })
            }
            getHotList(){
                //利用rap2请求数据渲染
                $.get('http://rap2.taobao.org:38080/app/mock/265925/api/index/goods',resp=>{
                    if (resp.code ===198){
                        const{ list } =resp.body
                        $('#HotList').html(template('HotListTemplate',{list:list.slice(0,4)}))
                        $('#goodList').html(template('HotListTemplate',{list:list.slice(4,8)}))
                    }
                })
            }
            getNewList(){
                $.get('http://xiongmaoyouxuan.com/api/tab/12',resp=>{
                    const { list } = resp.data.items
                    $('#newList').html(template('otherListTemplate',{list:list.slice(0,4)}))
                })
            }
        }
        new Index()
    })
})