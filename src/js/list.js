require(['./config'],()=>{
    require(['template','header','footer','backtotop'],(template)=>{
        class Index{
            constructor(){
                this.render()
            }
            render(){
                //从地址栏获取id，根据id发送请求，获取到数据再来渲染商品列表
                const id = location.search.slice(4)
                if (id.indexOf("=") === -1){
                    $.get(`http://rap2.taobao.org:38080/app/mock/265925/api/index/goods/${id}`,resp=>{
                        console.log(resp)
                    const {list} = resp.body,
                             {cname} = resp.body
                    $('#listATitle').html(template('listATitleTemplate',{cname:cname})),
                    $('#listTitle').html(template('listTitleTemplate',{cname:cname})),
                    $('#goodsList').html(template('goodsListTemplate',{list}))
                })
                }else{
                    const xid = location.search.slice(5)
                    $.get(`http://www.xiongmaoyouxuan.com/api/tab/${xid}`,resp=>{
                        const { list } = resp.data.items
                        const { cname } = resp.data.category
                        $('#listATitle').html(template('listATitleTemplate',{cname})),
                        $('#listTitle').html(template('listTitleTemplate',{cname})),
                        $('#goodsList').html(template('goodsList2Template',{list}))
                    })
                }
            }
        }
        new Index()
    })
})