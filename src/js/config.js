require.config({
    baseUrl:'/',
    paths:{
        jquery:'libs/jquery/jquery-1.11.3.min',
        header:'js/modules/header',
        footer:'js/modules/footer',
        backtotop:'js/modules/backtotop',
        template:'libs/art-template/template-web',
        elevateZoom:'libs/elevateZoom/jquery.elevateZoom-3.0.8.min',
        fly: 'libs/jquery-plugins/jquery.fly.min'
    },
    shim: {
        elevateZoom: {
          deps: ['jquery']
        },
        fly: {
          deps: ['jquery']
        }
      }
})