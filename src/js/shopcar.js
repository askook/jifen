require(['./config'],()=>{
    require(['template','header','footer','backtotop'],(template)=>{
        class Shopcar{
            constructor(){
                this.render()
            }
            render(){
                $('#noData').hide()
                const cart = localStorage.getItem('cart')
                if(cart){
                    this.cart = JSON.parse(cart)
                    $('#hasData').html(
                        template(('shopCarAllTemplate'),{cart:this.cart})
                    )
                    this.cartOperate()
                }else{
                    $('#hasData').hide()
                    $('#noData').show()
                }
            }
            cartOperate(){
                //cart有数据的时候调用
                this.setAllCheckStatus()//设置全选
                this.calcTotaMoney()//计算总价
                this.checksChange()
                this.allCheckChange()
                this.reOrAdd()
                this.deleteGood()
                this.allCheap()//计算总优惠
                this.allNum()
            }
            allNum(){
                const allNum = this.cart.reduce((total,shop)=>{
                    if(shop.check) total += shop.count
                    return total
                },0)
                $('.allNum').html(allNum)
            }
            allCheap(){
                const cheapMoney = this.cart.reduce((total,shop)=>{
                    if(shop.check) total += (shop.tbOriginPrice-shop.price)*shop.count
                    return total
                },0)
                $('.cheapALL').html(cheapMoney.toFixed(2))
            }
            setAllCheckStatus(){
                //单个导致全选
                const isAllcheck = this.cart.every(shop => shop.check)
                $('#itemAllCheck').prop('checked',isAllcheck)
            }
            allCheckChange(){
                //全选按钮
                const _this = this
                $('#itemAllCheck').on('click',function(){
                    const isCheck = $(this).prop('checked')
                    $('.smCheck').prop('checked',isCheck)
                    const ada = _this.cart.map((shop)=>{
                        shop.check = isCheck
                        return shop
                    })
                    localStorage.setItem('cart',JSON.stringify(ada))
                    _this.calcTotaMoney()
                })
            }
            calcTotaMoney(){
                //计算购物车总价
                const money = this.cart.reduce((total,shop)=>{
                    // console.log(shop.check)
                    if(shop.check) total += shop.price*shop.count
                    return total
                },0)
                $('#priceNum').html(money.toFixed(2))
            }
            checksChange(){
                const _this = this
                $('.smCheck').on('change',function(){
                    const isCheck = $(this).prop('checked')
                    const id = $(this).parents('tr').data('id')
                    //根据id修改this.cart
                    this.cart = _this.cart.map(shop=>{
                        if(shop.id === id) shop.check = isCheck
                        return shop
                    })
                    // console.log(this.cart)
                    localStorage.setItem('cart',JSON.stringify(_this.cart))
                    
                    //修改DOM
                    _this.calcTotaMoney()//计算总价
                    _this.allCheap()
                    _this.allNum()
                })
            }
            deleteGood(){
                const _this = this
                $('.delete').on('click',function(){
                    const id = $(this).parents('tr').data('id')
                    this.cart = _this.cart.filter(shop=>{
                        return shop.id !== id
                    })
                    console.log(this.cart)
                    if(this.cart.length === 0){
                        localStorage.removeItem('cart')
                    }else{
                        console.log(this.cart)
                        localStorage.setItem('cart',JSON.stringify(this.cart))
                    }
                    _this.calcTotaMoney()
                    _this.render()
                })
            }
            reOrAdd(){
                const _this = this
                $('.re').on('click',function(){
                    const numId = $(this).parents('tr').data('id')
                    this.cart = _this.cart.map(shop=>{
                        if(shop.id === numId) {
                            if(shop.count === 1){
                                _this.deleteGood()
                            }else{
                                shop.count--
                            }
                        }
                        return shop
                    })
                    console.log(this.cart)
                    localStorage.setItem('cart',JSON.stringify(_this.cart))
                    _this.calcTotaMoney()//计算总价
                    _this.render()
                    _this.allNum()
                })
                $('.add').on('click',function(){
                    const numId = $(this).parents('tr').data('id')
                    this.cart = _this.cart.map(shop=>{
                        if(shop.id === numId) {
                            if(shop.count !== 9999) shop.count++
                        }
                        return shop
                    })
                    localStorage.setItem('cart',JSON.stringify(_this.cart))
                    _this.calcTotaMoney()//计算总价
                    _this.render()
                    _this.allNum()
                })
                $('.input-add').attr("disabled",true);  
            }
        }
        new Shopcar()
    })
})