require(['./config'],()=>{
    require(['template','header','elevateZoom','fly','footer','backtotop'],(template,header)=>{
        class Detail{
            constructor(){
                this.render().then(()=>{
                    this.elevteZoom()
                    this.addOrReduce()
                    this.addToCart()
                }) 
            }
            render(){
                //从地址栏取到id，根据id向后端发送请求获取数据渲染页面
                const id = location.search.slice(4)
                if(id.indexOf("=") === -1){
                    return new Promise((resolve,reject)=>{
                        $.get('http://rap2.taobao.org:38080/app/mock/265925/example/1599621877397',{id},resp=>{
                            const{data} = resp
                        $('#details-all').html(
                            template('details-all2-template',{data})
                        )
                        resolve()
                        })
                    })
                }else{
                    const id = location.search.slice(5)
                    //如果用的是rap2，需要手动把地址栏上的的id加到返回的数据里
                    return new Promise((resolve,reject)=>{
                        $.get('http://xiongmaoyouxuan.com/api/detail',{id},resp=>{
                            const {
                                id,
                                photo,
                                title,
                                type,
                                saleNum,
                                qunTitle,
                                tbOriginPrice,
                                originPrice,
                                price,
                                descContentList
                            } = resp.data.detail
                            this.detail = {
                                id,
                                photo,
                                title,
                                type,
                                saleNum,
                                qunTitle,
                                tbOriginPrice,
                                originPrice,
                                price,
                                descContentList
                            }
                            $('#details-all').html(
                                template('details-all-template',{
                                    ...this.detail
                                })
                            )
                            //resolve放在异步代码回调函数里的最后一行
                            resolve()
                        })
                    })
                }
            }
            elevteZoom(){
                $('#main-img').elevateZoom({
                    gallery:'gal',
                }),
                // console.log($('.smallPic'))
                $('.smallPic').on('mouseenter',function(){
                    $(this).trigger("click")
                    // console.log($(this))
                })
            }
            addToCart(){
                //使用localStorage存
                $('#add-to-shopcar-btn').on("click",()=>{
                    const num = $('.quantity-input-tx').val()
                    let cart = localStorage.getItem('cart')
                    //判断是否存在数据
                    if(cart){
                        cart = JSON.parse(cart)
                        const isExist = cart.some(shop=>{
                            return shop.id === this.detail.id
                        })
                        if(isExist){
                            cart = cart.map(shop =>{
                                if(shop.id === this.detail.id) shop.count= parseInt(num)+parseInt(shop.count)
                                //无论是否满足if条件都需要写return
                                return shop
                            })
                        }else{
                            cart.push({...this.detail,count:1,check:true})
                        }
                        localStorage.setItem('cart',JSON.stringify(cart))
                    }else{
                        localStorage.setItem('cart',JSON.stringify([{
                            ...this.detail,
                            count:num,
                            check:true
                        }]))
                        console.log(count)
                    }
                })
                $('#add-to-shopcar-btn').on('click', function (e) {
                    $('<div class="dongxi">商品们</div>').fly({
                      start:{
                        left: e.clientX,  //开始位置（必填）#fly元素会被设置成position: fixed
                        top: e.clientY,  //开始位置（必填）
                      },
                      end:{
                        left: $('.shop-car').offset().left, //结束位置（必填）
                        top: $('.shop-car').offset().top,  //结束位置（必填）
                      },
                      // autoPlay: false, //是否直接运动,默认true
                      speed: 1.1, //越大越快，默认1.2
                      vertex_Rtop: 50, //运动轨迹最高点top值，默认20
                      onEnd: function(){
                        // 移除fly
                        this.destroy()
                        header.calcCartCount()
                      } 
                    })
                  })
            }
            addOrReduce(){
                let num = $('.quantity-input-tx').val()
                $('.add').on('click',()=>{
                    if(num !=9999 ){
                        num = parseInt(num)+1
                        $('.quantity-input-tx').val(num)
                        }
                    }),
                $('.reduce').on('click',()=>{
                    if (num != 1){
                        num = parseInt(num)-1
                        $('.quantity-input-tx').val(num)
                    }
                })
                $('.quantity-input-tx').blur(()=>{
                    const value = $('.quantity-input-tx').val()
                    if(value){
                        num = value
                    }
                    else{
                        console.log(2)
                        $('.quantity-input-tx').val(1)
                    }
                })
            }
        }
        new Detail()
    })
})