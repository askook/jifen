const gulp = require('gulp'),
      htmlmin = require('gulp-htmlmin'),
      cleanCss = require('gulp-clean-css'),
      autoprefixer = require('gulp-autoprefixer'),
      uglify = require('gulp-uglify'),
      babel = require('gulp-babel'),
      del = require('del'),
      connect = require('gulp-connect'),
      sass = require('gulp-sass')

// 制定一个删除dist的任务
const delDist = () => del('dist')

// 把所有任务相关的路径做一个集中管理
const paths = {
  html: {
    src: 'src/**/*.html',
    dest: 'dist'
  },
  css: {
    src: 'src/css/**/*.scss',
    dest: 'dist/css'
  },
  js: {
    src: 'src/js/**/*.js',
    dest: 'dist/js'
  },
  imgs: {
    src: 'src/imgs/**',
    dest: 'dist/imgs'
  },
  libs: {
    src: 'src/libs/**',
    dest: 'dist/libs'
  }
}

// 制定html任务：对html文件完成压缩放入dist目录里
const html = () => {
  // ** 代表所有目录，*代表所有文件
  return gulp.src(paths.html.src)
    .pipe(htmlmin({
      removeComments: true, // 清除HTML注释
      collapseWhitespace: true, // 压缩HTML
      collapseBooleanAttributes: true, // 省略布尔属性的值 <input checked="true"/> ==> <input checked />
      removeEmptyAttributes: true,//删除所有空格作属性值 <input id="" /> ==> <input />
      removeScriptTypeAttributes: false, // 删除<script>的type="text/javascript"
      removeStyleLinkTypeAttributes: true, // 删除<style>和<link>的type="text/css"
      minifyJS: true, // 压缩页面JS
      minifyCSS: true // 压缩页面CSS 
    }))
    .pipe(gulp.dest(paths.html.dest))
    .pipe(connect.reload())
}

// 制定css任务：先把scss编译成css，再加上兼容性前缀，最后压缩css文件
const css = () => {
  return gulp.src(paths.css.src)
    .pipe(sass())
    .pipe(autoprefixer({
      cascade: false
    }))
    .pipe(cleanCss())
    .pipe(gulp.dest(paths.css.dest))
    .pipe(connect.reload())
}

// 制定js任务：先ES6转ES5，再压缩混淆js代码
const js = () => {
  return gulp.src(paths.js.src)
    .pipe(babel({
      presets: ['@babel/env']
    }))
    .pipe(uglify())
    .pipe(gulp.dest(paths.js.dest))
    .pipe(connect.reload())
}

// 制定imgs任务：移动图片
const imgs = () => gulp.src(paths.imgs.src).pipe(gulp.dest(paths.imgs.dest))

// 制定libs任务：移动文件
const libs = () => gulp.src(paths.libs.src).pipe(gulp.dest(paths.libs.dest))

// 制定server任务：在dist目录里开启服务器
const server = () => {
  connect.server({
    root: 'dist',
    port: 3399,
    livereload: true,
  })
}

// 制定watch任务：监听文件修改自动重启对应任务
const watch = () => {
  // 监听src里html文件的修改，自动重启html任务
  gulp.watch(paths.html.src, html)
  gulp.watch(paths.css.src, css)
  gulp.watch(paths.js.src, js)
}

// 只导出default这一个任务，这一个任务里先同步删除dist，再异步执行其他所有任务
// parallel 异步执行任务
// series   同步执行任务
module.exports.default = gulp.series(delDist, gulp.parallel(html, css, js, imgs, libs,server,watch))



// module.exports = {
//   html,
//   css,
//   js
// }
